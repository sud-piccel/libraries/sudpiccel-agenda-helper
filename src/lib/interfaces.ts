/**
 * @interface IEventDate Represents an event date as written in the Agenda's spreadsheet.
 */
export interface IEventDate {
  slug: string;
  announced: boolean;
  title: string;
  description: string;
  prerequisites: string;
  thumbnail: string;
  subjects: string[];
  format: string;
  guests: string[];
  start: Date;
  end: Date;
  locationName: string;
  locationAddress: string;
  locationAccess: string;
  prices: string;
  url: string;

  // Bonus properties
  /**
   * @property Defines if this event is valid, so it's announced and not outdated
   */
  isValid: boolean;

  /**
   * @property Defines if this event is outdated
   */
  isPassed: boolean;

}
