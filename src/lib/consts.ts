/**
 * @constant EVENTS_HEADERS The event data headers, as defined in the Agenda spreadsheet.
 */
export const EVENTS_HEADERS = [
  'slug',
  'announced',
  'title',
  'description',
  'prerequisites',
  'thumbnail',
  'subjects',
  'format',
  'guests',
  'start',
  'end',
  'locationName',
  'locationAddress',
  'locationAccess',
  'prices',
  'url'
];

/**
 * @constant GSHEET_TRUE Defines the boolean value "true", as written in the Agenda spreadsheet.
 */
export const GSHEET_TRUE = 'TRUE';

/**
 * @constant AGENDA_SPREADSHEET_ID Defines the file id of the Agenda spreadsheet.
 */
export const AGENDA_SPREADSHEET_ID = '1haOD_z4WgqpqfALLLmeOwpa6o1YFXLNGEKFMvLo0b2o';

/**
 * @constant AGENDA_SPREADSHEET_NAME Defines the name of the Agenda spreadsheet.
 */
export const AGENDA_SPREADSHEET_NAME = 'Agenda';

/**
 * @constant MIN_HORIZONTAL_RANGE Defines the minimum range (horizontally) of a read query in the Agenda spreadsheet.
 */
export const MIN_HORIZONTAL_RANGE = 'B';

/**
 * @constant MAX_HORIZONTAL_RANGE Defines the maximm range (horizontally) of a read query in the Agenda spreadsheet.
 */
export const MAX_HORIZONTAL_RANGE = 'Q';

/**
 * @constant MIN_VERTICAL_RANGE Defines the minimum range (vertically) of a read query in the Agenda spreadsheet.
 */
export const MIN_VERTICAL_RANGE = 3;
