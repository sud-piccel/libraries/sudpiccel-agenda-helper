import GoogleAPIManager from '@muffin-dev/google-helpers';
import { Credentials } from 'google-auth-library';
import { ICredentialsData, TCredentialsType } from '@muffin-dev/google-helpers/lib/auth';
import { IEventDate, EVENTS_HEADERS, GSHEET_TRUE } from '.';
import { AGENDA_SPREADSHEET_ID, AGENDA_SPREADSHEET_NAME, MAX_HORIZONTAL_RANGE, MIN_HORIZONTAL_RANGE, MIN_VERTICAL_RANGE } from './consts';

export class AgendaHelper {

  /**
   * @property Contains the GoogleAPIManager instance used to read events data from the Agenda spreadsheet.
   */
  private _googleAPIManager: GoogleAPIManager = null;

  //#region Initialization

  /**
   * Class constructor.
   */
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  private constructor() { }

  /**
   * Creates a new AgendaHelper instance, using the given credentials to read the Agenda spreadsheet.
   * @param credentials The loaded credentialsyou want to use (from credentials.json file, which you can download from Google API Console).
   * @param token The loaded token (from token.json file, which you can generate using the generateToken command).
   */
  public static async create(credentials: ICredentialsData, token: Credentials): Promise<AgendaHelper>;

  /**
   * Creates a new AgendaHelper instance, using the given credentials to read the Agenda spreadsheet.
   * @param credentialsFilePath The path to the credentials.json file you want to use (which you can download from Google APIs Console).
   * @param tokenFilePath The path to your token.json file (which you can generate using the generateToken command).
   * @param credentialsType Defines the types of credentials you want to use from the credentials.json file (depending on your app's target
   * device).
   */
  public static async create(
    credentialsFilePath: string,
    tokenFilePath: string,
    credentialsType: TCredentialsType
  ): Promise<AgendaHelper>;

  /**
   * Creates a new AgendaHelper instance, using the given Google API Manager instance to read the Agenda spreadsheet.
   * @param credentialsFilePath The path to the credentials.json file you want to use (which you can download from Google APIs Console).
   * @param tokenFilePath The path to your token.json file (which you can generate using the generateToken command).
   * @param credentialsType Defines the types of credentials you want to use from the credentials.json file (depending on your app's target
   * device).
   */
  public static async create(googleAPIManager: GoogleAPIManager): Promise<AgendaHelper>;

  /**
   * Creates a new AgendaHelper instance, using the given credentials to read the Agenda spreadsheet.
   * @param credentials The loaded credentials or path to the credentials.json file (which you can download from Google APIs Console).
   * @param token The loaded token or path to your token.json file (which you can generate using the generateToken command).
   * @param credentialsType Defines the types of credentials you want to use from the credentials.json file (used only if you passed a file
   * path as the value of the credentials parameter).
   */
  public static async create(
    credentials: string | ICredentialsData | GoogleAPIManager,
    token: string | Credentials = null,
    credentialsType: TCredentialsType = null
  ): Promise<AgendaHelper> {
    const agendaHelper = new AgendaHelper();
    if (credentials && typeof credentials === 'object' && credentials instanceof GoogleAPIManager) {
      agendaHelper._googleAPIManager = credentials as GoogleAPIManager;
    }
    else {
      agendaHelper._googleAPIManager = await GoogleAPIManager.create(credentials as string, token as string, credentialsType);
    }
    return agendaHelper;
  }

  //#endregion


  //#region Public API

  /**
   * @async
   * Gets the given amount of events from the Agenda spreadsheet.
   * @param quantity The number of events you want to get.
   * @returns Returns the processed events.
   */
  public async getEvents(quantity: number): Promise<IEventDate[]>;

  /**
   * Gets the given amount of events from the Agenda spreadsheet.
   * @async
   * @param index The index of the first row you want to get.
   * @param quantity The number of events you want to get.
   * @returns Returns the processed events.
   */
  public async getEvents(index: number, quantity: number): Promise<IEventDate[]>;

  /**
   * Gets the given amount of events from the Agenda spreadsheet.
   * @async
   * @param qtyOrIndex The index of the first row or the number of events you want to get.
   * @param quantity The number of events you want to get (used only if the first parameter represents an index).
   * @returns Returns the processed events.
   */
  public getEvents(qtyOrIndex: number, quantity: number = null): Promise<IEventDate[]> {
    if (quantity === null) {
      quantity = qtyOrIndex;
      qtyOrIndex = MIN_VERTICAL_RANGE;
    }
    return this._getEvents(qtyOrIndex, quantity, false);
  }

  /**
   * @async
   * Gets the given amount of valid events (announced and not outdated) from the Agenda spreadsheet.
   * @param quantity The number of events you want to get.
   * @returns Returns the processed events.
   */
  public async getValidEvents(quantity: number): Promise<IEventDate[]>;

  /**
   * @async
   * Gets the given amount of valid events (announced and not outdated) from the Agenda spreadsheet.
   * @param index The index of the first row you want to get.
   * @param quantity The number of events you want to get.
   * @returns Returns the processed events.
   */
  public async getValidEvents(index: number, quantity: number): Promise<IEventDate[]>;

  /**
   * @async
   * Gets the given amount of valid events (announced and not outdated) from the Agenda spreadsheet.
   * @param qtyOrIndex The index of the first row or the number of events you want to get.
   * @param quantity The number of events you want to get (used only if the first parameter represents an index).
   * @returns Returns the processed events.
   */
  public getValidEvents(qtyOrIndex: number, quantity: number = null): Promise<IEventDate[]> {
    if (quantity === null) {
      quantity = qtyOrIndex;
      qtyOrIndex = MIN_VERTICAL_RANGE;
    }
    return this._getEvents(qtyOrIndex, quantity, true);
  }

  public static fromRawData(data: string[]): IEventDate {
    const eventData: Record<string, unknown> = { };

    // eslint-disable-next-line unicorn/no-for-loop
    for (let i = 0; i < EVENTS_HEADERS.length; i++) {
      switch (EVENTS_HEADERS[i]) {
        case 'announced':
          eventData[EVENTS_HEADERS[i]] = data[i] === GSHEET_TRUE;
          break;

        case 'subjects':
        case 'guests':
          eventData[EVENTS_HEADERS[i]] = this._toStringArray(data[i]);
          break;

        case 'start':
          eventData[EVENTS_HEADERS[i]] = this._parseDate(data[i]);
          break;

        case 'end':
          eventData[EVENTS_HEADERS[i]] = this._parseDate(data[i]);
          break;

        default:
          eventData[EVENTS_HEADERS[i]] = data[i] !== '' ? data[i] : null;
          break;
      }
    }

    eventData.isPassed = this._isPassed((eventData as unknown) as IEventDate);
    eventData.isValid = this._isValid((eventData as unknown) as IEventDate);

    return (eventData as unknown) as IEventDate;
  }

  //#endregion


  //#region Private methods

  /**
   * Parses the given date string.
   * @param dateValue The raw date value, as written in the Agenda spreadsheet.
   */
  private static _parseDate(dateValue: string): Date {
    if (dateValue === '') {
      return null;
    }

    // NOTE: dates from the Agenda should have this format : dd/MM/YYYY hh:mm
    const parts = dateValue.split(' ');
    const dateSplit = parts[0].split('/');
    const day = parseInt(dateSplit[0], 10) || 0;
    const month = (parseInt(dateSplit[1], 10) - 1) || 0;
    const year = parseInt(dateSplit[2], 10) || 0;
    let hours = 0;
    let minutes = 0;

    // If the hour is specified
    if (parts.length >= 2) {
      const hourSplit = parts[1].split(':');
      hours = parseInt(hourSplit[0], 10) || 0;
      minutes = parseInt(hourSplit[1], 10) || 0;
    }

    return new Date(year, month, day, hours, minutes);
  }

  /**
   * Spits the given value using the comma ( , ) separator.
   * @param data The raw data from the Agenda spreadsheet.
   */
  private static _toStringArray(data: string): string[] {
    if (!data || data === '') {
      return [];
    }

    const split = data.split(',');
    for (let i = 0; i < split.length; i++) {
      split[i] = split[i].trim();
    }
    return split;
  }

  /**
   * Checks if this event is outdated.
   * @param evt The event you want to check.
   */
  private static _isPassed(evt: IEventDate): boolean {
    if (evt.end) {
      return Date.now() > evt.end.valueOf();
    }
    else if (evt.start) {
      return Date.now() > evt.start.valueOf();
    }
    else {
      return true;
    }
  }

  /**
   * Checks if this event is announced and not outdated.
   * @param evt The event you want to check.
   */
  private static _isValid(evt: IEventDate): boolean {
    if (evt.isPassed === undefined) {
      evt.isPassed = this._isPassed(evt);
    }
    return evt.announced && !evt.isPassed;
  }

  /**
   * @async
   * Gets the events using the given criteria.
   * @param index The row index of the first event you want to get.
   * @param quantity The number of events from the given index you want to get.
   * @param filterValid If true, only the valid events (announced and not outdated) will be processed.
   * @returns Returns the processed events.
   */
  private async _getEvents(index: number, quantity: number, filterValid: boolean): Promise<IEventDate[]> {
    const rawData = await this._readRows(index, quantity);
    if (rawData === null) {
      return [];
    }

    const events = new Array<IEventDate>();
    for (const data of rawData) {
      const evt = AgendaHelper.fromRawData(data);
      if (!filterValid || evt.isValid) {
        events.push(evt);
      }
    }
    return events;
  }

  /**
   * Gets the events rows from the Agenda spreadsheet.
   * @param index The index of the first row of the range.
   * @param quantity The number of rows you want to get from the given index.
   */
  private async _readRows(index: number, quantity: number): Promise<string[][]> {
    index = Math.max(MIN_VERTICAL_RANGE, index);
    const fromCoords = `${MIN_HORIZONTAL_RANGE}${index}`;
    const toCoords = `${MAX_HORIZONTAL_RANGE}${index + quantity}`;
    return await this._googleAPIManager.spreadsheets.read(
      AGENDA_SPREADSHEET_ID, `${AGENDA_SPREADSHEET_NAME}!${fromCoords}:${toCoords}`
    ) as string[][] || null;
  }

  //#endregion

}
